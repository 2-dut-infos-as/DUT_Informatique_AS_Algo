public class ABR extends ArbreBin {


    ABR() {
        vide = true;
    }


    ABR(int info, ABR fg, ABR fd) {
        vide = false;
        this.info = info;
        this.fg = fg;
        this.fd = fd;
    }

    //Minimum 1
    public int min() {

        if (this.isVide()) {
            return -1;
        } else return this.min();
    }

    //Minimum 2

    public int min2() {

        if (this.fg.isVide()) {
            return this.getinfo();
        } else return ((ABR) this.fg).min2();
    }

    //Maximum
    public int max() {

        if (this.isVide()) {
            return -1;
        } else return this.max();
    }

    //Maximum 2
    public int max2() {

        if (this.fd.isVide()) {
            return this.getinfo();
        } else return ((ABR) this.fg).max2();
        //Je cast l'ABR pour le fg
    }


    //Ajouter valeur
    public void ajout(int uneVal) {
        //ajoute une valeur `a un arbre binaire de recherche

        if (this.isVide()) {
            this.creation(uneVal);
        } else if (uneVal <= this.getinfo()) {
            ((ABR) this.fg).ajout(uneVal);
        } else ((ABR) this.fd).ajout(uneVal);
    }


    //mprocedure cr�ation
    public void creation(int uneVal) {
        //cree un arbre binaire r eduit `a sa racine et y affecte une Val
    }


    //Pr�decesseur
    public int pred(int cle) {
        if (this.isVide()) {
            return -1;
        } else return (this.predBis(cle, cle + 1));
    }

    private int predBis(int cle, int preprov) {

        if (this.getinfo() > cle) {
            return ((ABR) this.fg).predBis(cle, preprov);
        } else if (this.getinfo() < cle) {
            return ((ABR) this.fg).predBis(cle, this.getinfo());
        } else if (this.fg.isVide()) {
            return (preprov);
        } else return ((ABR) this.fg).max();
    }


    public static void main(String[] args) {
        // TODO Auto-generated method stub
        Liste l = new Liste();


    }

}

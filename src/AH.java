import java.util.HashMap;

public class AH {
    protected boolean vide;
    protected int frequence;
    protected AH fg;
    protected AH fd;
    protected String lettre;

    //Constructeur 1
    AH() {
        vide = true;
    }

    //Constructeur 2
    AH(String lettre, int frequence) {
        vide = false;
        this.frequence = frequence;
        this.lettre = lettre;
        this.fg = new AH();
        this.fd = new AH();
    }

    //Constructeur 3
    AH(AH fg, AH fd) {
        vide = false;
        this.fg = fg;
        this.fd = fd;
        this.frequence = fd.getFrequence() + fg.getFrequence();
        this.lettre = "";
    }

    //retourne vrai si la liste est vide, faux sinon
    public boolean isVide() {
        return vide;
    }

    //
    public boolean isVide(int frq) {
        return vide;
    }

    //fils gauche
    public AH filsgauche() {
        if (this.isVide()) {
            return null;
        }
        return fg;
    }

    //fils droit
    public AH filsdroit() {
        if (this.isVide()) {
            return null;
        }
        return fd;
    }

    //Getter de la fr�auence
    public int getFrequence() {
        return frequence;
    }


    //Getter Lettre
    public String getLettre() {
        return lettre;
    }

    //affichageInfixe
    public void AffichageInfixe() {
        // AffichageInfixe(ArbreBin unArbre)
        if (!this.isVide()) {

            System.out.print("(");
            this.filsgauche().AffichageInfixe();
            //AffichageInfixe(unArbre.filsgauche())
            System.out.print(" " + this.getFrequence() + " " + this.getLettre());
            this.filsdroit().AffichageInfixe();
            //AffichageInfixe(unArbre.filsDroit())
            System.out.print(")");
        }
    }


    public static void main(String[] args) {


    }

}





public class ArbreBin {
    protected boolean vide;
    protected int info;
    protected ArbreBin fg;

    protected ArbreBin fd;

    //Constructeur 1
    ArbreBin() {
        vide = true;
    }

    //Constructeur 2
    ArbreBin(int info, ArbreBin fg, ArbreBin fd) {
        vide = false;
        this.info = info;
        this.fg = fg;
        this.fd = fd;
    }

    //Constructeur 3
    ArbreBin(int info) {
        vide = false;
        this.info = info;
        //cr�� le fils gauche et fils droit
        fg = new ArbreBin();
        fd = new ArbreBin();
    }

    //retourne vrai si la liste est vide, faux sinon
    public boolean isVide() {
        return vide;
    }

    //fils gauche
    public ArbreBin filsgauche() {
        return fg;
    }

    //fils droit
    public ArbreBin filsdroit() {
        return fd;
    }

    //La racine
    public int racine() {
        return info;
    }

    public int getinfo() {
        return info;
    }

    //Fonction Max
    public int SommeVal() {
        if (this.isVide()) {
            return 0;
        } else {
            return (this.getinfo() + this.filsgauche().SommeVal() +
                    this.filsdroit().SommeVal());
        }
    }

    //fonction profondeur
    public int profondeur() {
        if (this.isVide()) {
            return -1;
        } else {
            return (1 + maxdeuxentier(this.filsgauche().profondeur(), this.filsdroit().profondeur()));
        }
    }

    //Fonction Max
    public int maxdeuxentier(int a, int b) {
        if (this.isVide()) {
            return -1;
        } else {
            return ((a > b) ? a : b);
        }
    }


    //affichageInfixe
    public void AffichageInfixe() {
        // AffichageInfixe(ArbreBin unArbre)
        if (!this.isVide()) {

            System.out.print("(");
            this.filsgauche().AffichageInfixe();
            //AffichageInfixe(unArbre.filsgauche())
            System.out.print(this.racine());
            this.filsdroit().AffichageInfixe();
            //AffichageInfixe(unArbre.filsDroit())
            System.out.print(")");
        }
    }

    //Fonction contenu
    public static boolean contenu(Liste L, ArbreBin A) {
        if (A.isVide()) {
            return true;
        } else if (A.getinfo() == L.getTete()) {
            return (contenu(L.getReste(), A.filsgauche()) ||
                    contenu(L.getReste(), A.filsdroit()));
        } else return (contenu(L, A.filsgauche()) ||
                contenu(L, A.filsdroit()));
    }


    public static void main(String[] args) {
        Liste l = new Liste();


        ArbreBin a = new ArbreBin(7);
        ArbreBin b = new ArbreBin(9);
        ArbreBin c = new ArbreBin(4);
        ArbreBin d = new ArbreBin(10);

        ArbreBin e = new ArbreBin(3, a, b);
        ArbreBin f = new ArbreBin(5, c, d);
        ArbreBin g = new ArbreBin(1, e, f);


        g.AffichageInfixe();
        System.out.print("\n" + g.SommeVal());
        System.out.print("\n" + g.profondeur());
        System.out.print("\n" + contenu(l, g));

    }

}





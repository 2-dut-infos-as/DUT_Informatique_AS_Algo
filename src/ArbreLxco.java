public class ArbreLxco {

    protected ArbreLxco fg;
    protected ArbreLxco fd;
    protected char Lettre;
    protected boolean vide;
    protected boolean finMot;

    //Constructeur 2
    ArbreLxco() {
        vide = true;
    }

    //Constructeur 2
    ArbreLxco(char lettre) {
        vide = false;
        this.Lettre = lettre;
        fg = new ArbreLxco();
        fd = new ArbreLxco();
    }

    //Constructeur 3
    ArbreLxco(char lettre, ArbreLxco fg, ArbreLxco fd) {
        vide = false;
        this.Lettre = lettre;
        this.fg = fg;
        this.fd = fd;
    }

    //------------------------------------------------------
    //		GETTER
    //--------------------------------------------
    public ArbreLxco getFg() {
        return fg;
    }

    public ArbreLxco getFd() {
        return fd;
    }

    public char getLettre() {
        return Lettre;
    }


    //---------------------------------------------------------
    //				SETTER
    //---------------------------------------------------------
    public void setFg(ArbreLxco fg) {
        this.fg = fg;
    }

    public void setFd(ArbreLxco fd) {
        this.fd = fd;
    }

    public void setLettre(char lettre) {
        Lettre = lettre;
    }

    public void setVide(boolean vide) {
        this.vide = vide;
    }

    public void setFinMot(boolean finMot) {
        this.finMot = finMot;
    }

    public boolean isVide() {
        return vide;
    }

    public boolean isFinMot() {
        return finMot;
    }


    //---------------------------------------
    public ArbreLxco ajouterMot(String mot) {

        if (mot.length() > 1) {
            System.out.println(mot);
            this.Lettre = mot.charAt(0);
            this.finMot = false;
            this.fg = this.ajouterMot(mot.substring(1));
            this.fd = new ArbreLxco();
            this.vide = false;

            return this;

        } else {
            System.out.println(mot + "salut");
            this.Lettre = mot.charAt(mot.length() - 1);
            this.fg = new ArbreLxco();
            this.finMot = true;
            this.fd = new ArbreLxco();
            this.vide = false;

            return this;
        }


    }


    public boolean existe(String mot) {
        char a = mot.charAt(0);
        if (a == this.Lettre) {
            this.fg.existe(mot.substring(1));
        } else {
            this.fd.existe(mot);
        }

        if (this.isVide()) {
            return false;
        } else if (mot.length() == 1) {
            if (a == this.Lettre) {
                return this.finMot;
            } else {
                return this.fd.existe(mot);
            }
        } else if (a == this.Lettre) {
            return this.fg.existe(mot.substring(1));
        } else {
            return this.fd.existe(mot);
        }

    }


    public static void main(String[] args) {
        // TODO Auto-generated method stub

    }

}

import java.util.HashMap;

public class Codage {
    public static int TabFreq[] = {};
    public static String TabLettre[] = {};

    static int t;

    //Constructeur 1
    static public AH constr(ListeAH l) {
        if (l.getReste().isVide()) {
            return l.getTete();
        } else {
            AH a = new AH(l.getTete(), l.getReste().getTete());

            return constr(l.getReste().getReste().insererOrd(a));
        }
    }

    //Test pour l'affichage
    public static void codage_recup(AH arbre, String cod) {
        if (arbre.isVide()) {
            System.out.println("erreur");
        } else if (arbre.filsgauche().isVide() && arbre.filsdroit().isVide()) {
            //Tab[i][]=cod;
            //Tab[i][]=abr.getLettre();
            System.out.println(cod + ":" + arbre.getLettre());
        } else {
            codage_recup(arbre.filsgauche(), cod + "0");
            codage_recup(arbre.filsdroit(), cod + "1");
        }
    }


    //Construction d'un dictionnaire des codes et lettres
    //Entr�e : AH + code provisoire
    //Sortie : Dictionnaire
    public static void codage_recup(AH arbre, String cod, HashMap<String, String> hm) {
        if (arbre.isVide()) {
            System.out.println("erreur");
        } else if (arbre.filsgauche().isVide() && arbre.filsdroit().isVide()) {
            hm.put(arbre.getLettre(), cod);
        } else {
            codage_recup(arbre.filsgauche(), cod + "0", hm);
            codage_recup(arbre.filsdroit(), cod + "1", hm);
        }
    }


    private boolean isVide() {
        // TODO Auto-generated method stub
        return false;
    }

    //-----------------------------------------------------------------------
    //Codage de la chaine de caract�re
    public static String code(HashMap<String, String> hm, String texte) {

        String msg = "";

        for (int i = 0; i < texte.length(); i++) {

            //string a char
            char txt = texte.charAt(i);

            //stockage dans le string
            msg = msg + hm.get("" + txt);

            //Affichage du codage en concat�nant
            System.out.print(hm.get("" + txt));
            System.out.print("|");
        }
        return msg;

    }

    //-----------------------------------------------------------------------
    //D�codage de la chaine de caract�re
    public static String decode(AH arbre, String ChaineBin) {

        String mot = "";
        t = 0;
        int longueur = ChaineBin.length();

        while (t < longueur) {
            String c = trouverLettre(arbre, ChaineBin);
            mot = mot + c;
        }
        return mot;
    }

    //Trouve la lettre dans l'arbre, recursif
    public static String trouverLettre(AH arbre, String car) {


        //Si arbre vide
        if (arbre.fg.isVide() && arbre.fd.isVide()) {

            return arbre.getLettre();
        }
        //Sinon v�rification si le caract�re est �gal � 0
        else if (car.charAt(t) == '0') {

            t++;
            return trouverLettre(arbre.fg, car);
        }
        //Sinon returner la lettre trouver
        else {

            t++;
            return trouverLettre(arbre.fd, car);
        }
    }


    //---------------------------------------------------------------------------
    //Transformation du hashmap enTableau
    public static String[][] getArrayFromHash(HashMap<String, String> hashMap) {
        String[][] str = null;
        {
            //caract�re
            Object[] keys = hashMap.keySet().toArray();

            //codage par caract�re
            Object[] values = hashMap.values().toArray();
            str = new String[keys.length][values.length];

            for (int i = 0; i < keys.length; i++) {
                str[0][i] = (String) keys[i];
                str[1][i] = (String) values[i];
            }

        }
        return str;
    }
    //----------------------------------------------------------------------
    /*public int getFrequence() {
		return frequence;
	}*/

    public static void main(String[] args) {
        // TODO Auto-generated method stub

        ListeAH l = new ListeAH();


        int tabFrq[] = {};

        String tabLet[] = {};

        String obtenti = "toto";

        ObtentionFreq(tabFrq, tabLet, obtenti);

        for (int i = 0; i < tabLet.length; i++) {
            System.out.println(tabFrq[i] + "   " + tabLet[i]);
        }

        l = l.prefixer(new AH(tabLet[0], tabFrq[0]));
        for (int i = 1; i < tabLet.length; i++) {
            l = l.insererOrd(new AH(tabLet[i], tabFrq[i]));
        }


        l.afficherListePropre();

        AH z = constr(l);
        z.AffichageInfixe();

        System.out.println("");
        String tab[][];

        //--------------------------------------------------------
        //Codage de la chaine binaire
        codage_recup(z, "");

        HashMap hm_arbre = new HashMap();

        codage_recup(z, "", hm_arbre);

        System.out.println("On va coder cette chaine : cafe");

        String code = code(hm_arbre, "cafe");

        System.out.println();
        System.out.println("\ncodage de la variable : code <= cafe ");
        System.out.println("resultat du codage :" + code);
        System.out.println();

        //Cr�ation de tableau de string <== hashmap
        String[][] test = getArrayFromHash(hm_arbre);

        //Affichage du tableau (verification)
        for (int i = 0; i < test.length; i++) {

            System.out.print(test[0][i] + ":");
            System.out.print(test[1][i] + "\n");
        }

        //-------------------------------------------------
        //d�codage de la chaine de caract�re

        String chaine_code = "11001111010";
        String deco = decode(z, chaine_code);
        System.out.println();
        System.out.println("Codage de : " + chaine_code);
        System.out.println("Resultat du decodage : " + deco);

        //------------------------------------------------
        //Codage de la chaine binaire
        codage_recup(z, "");

        HashMap hm_arbre1 = new HashMap();

        codage_recup(z, "", hm_arbre);

        System.out.println("On va coder la phrase");

        String code2 = code(hm_arbre, "Le chat mange la souris.La souris est mang�e par le chat vorace. La porte de la chambre ferm�e � clef � l' int�rieur , les volets de l'unique fen�tre ferm�s , eux aussi , � l' int�rieur , et , par-dessus les volets , les barreaux intacts , des barreaux �_travers lesquels vous n' auriez pas pass� le bras ... et mademoiselle qui appelait au secours ! ... ou plut�t non , on ne l' entendait plus ... elle �tait peut-�tre morte ... mais j' entendais encore , au fond du pavillon , monsieur qui essayait d' �branler la porte ... nous avons repris notre course , la concierge et moi , et nous sommes revenus au pavillon");

        System.out.println();
        System.out.println("\ncodage de la variable : code <= cafe ");
        System.out.println("resultat du codage :" + code);
        System.out.println();

        //Cr�ation de tableau de string <== hashmap
        String[][] test2 = getArrayFromHash(hm_arbre);

        //Affichage du tableau (verification)
        for (int i = 0; i < test2.length; i++) {

            System.out.print(test2[0][i] + ":");
            System.out.print(test2[1][i] + "\n");
        }


    }

    private static void ObtentionFreq(int[] tabFrq, String[] tabLet, String obtenti) {
        // TODO Auto-generated method stub

    }
}




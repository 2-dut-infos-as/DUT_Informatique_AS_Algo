import java.util.ArrayList;

public class Codage_correction {

    //Construction de l arbre a partir de la liste d AH
    static public AH constr(ListeAH l) {
        if (l.getReste().isVide()) {
            return l.getTete();
        } else {
            AH a = new AH(l.getTete(), l.getReste().getTete());
            l = l.getReste().getReste();
            l = l.insererOrd(a);
            return constr(l);
        }
    }

    //Construction du Dictionnaire
    public static void dico(AH arbre, ArrayList<String> tabB, ArrayList<String> tabL, String code) {

        //Si c est une feuille on ajoute le caractere et le code correspondant dans deux arrayLists
        if ((arbre.fg.isVide()) && (arbre.fd.isVide())) {
            tabB.add(code);
            tabL.add(arbre.getLettre());


        } else {
            dico(arbre.fg, tabB, tabL, code + '0');
            dico(arbre.fd, tabB, tabL, code + '1');
        }
    }

    // Coder un message msg
    public static String code(ArrayList<String> dicoBites, ArrayList<String> dicoLetter, String msg) {
        String newCode = "";
        //Pour chaque lettre du msg a coder
        for (int j = 0; j < msg.length(); j++) {
            char car = msg.charAt(j);
            int i = 0;

            //On cherche notre lettre dans le dictionnaire
            while (i < dicoBites.size()) {
                String leCode = dicoLetter.get(i);


                if (car == leCode.charAt(0)) {


                    newCode = newCode + dicoBites.get(i);

                }
                i++;
            }
        }
        return newCode;

    }

    //Variable statique pour connaitre la position dans la chaine de bits
    private static int t = 0;

    //Decode la chaine de bits
    public static String decode(AH arbre, String ChaineB) {

        String mot = "";
        t = 0;
        int longueur = ChaineB.length();
        //Tant qu il reste des bits on va cherche les lettre dans l arbre
        while (t < longueur) {
            String c = trouverLettre(arbre, ChaineB);
            mot = mot + c;
        }
        return mot;
    }

    //Trouve la lettre dans l'arbre, recursif
    public static String trouverLettre(AH arbre, String car) {


        //Feuille :
        if (arbre.fg.isVide() && arbre.fd.isVide()) {

            return arbre.getLettre();
        }
        // Si bit est 0 on va chercher dans le fils gauche
        else if (car.charAt(t) == '0') {

            t++;
            return trouverLettre(arbre.fg, car);
        }
        // Si bit est 1 on va chercher dans le fils droit

        else {

            t++;
            return trouverLettre(arbre.fd, car);
        }
    }

    //Construction de l arbre a partir de texte--------------------------------------
    public static AH constrArbre(String texte) {


        if (texte != null) {


            int tabFreq[] = new int[10000];

            //tabLettre mais qui contiendra des caracteres speciaux (espaces, point, etc...)
            String tabLettre[] = new String[10000];

            int longueur = texte.length();
            int longueurTab = 0;
            boolean trouve = false;
            int j = 0;

            //pour chaque caractere du texte donn�
            for (int i = 0; i < longueur; i++) {

                //tant qu on a pas trouv� dans le tabl lettre
                while (!trouve && j < longueurTab) {
                    trouve = texte.charAt(i) == tabLettre[j].charAt(0);
                    j++;
                }

                //Si il est pas present dans notre tableau de caractere, on l ajoute et on initialise sa frequence
                if (!trouve) {
                    tabLettre[j] = "" + texte.charAt(i);
                    tabFreq[j] = 1;
                    longueurTab = longueurTab + 1;
                }

                //Si il est deja dans le tableau on augmente sa frequence
                else {
                    tabFreq[j - 1] = tabFreq[j - 1] + 1;
                }

                //Pour chaque nouvelle lettre du texte on initialise trouve et j
                trouve = false;
                j = 0;
            }

            for (int z = 0; z < longueurTab; z++) {
                System.out.println("[" + tabFreq[z] + "=" + tabLettre[z] + "]");
            }

            System.out.println("Il y a " + longueurTab + " lettres diff�rentes dans le texte");

            //Construction de l arbre
            //On cree la liste d arbre
            ListeAH l = new ListeAH();

            //On prefique la liste avec l'arbre qui contient la premiere case des deux tableaux
            AH a = new AH(tabLettre[0], tabFreq[0]);
            l = l.prefixer(a);

            //Puis on insere dans laliste de maniere ordonn� tous les arbres
            for (int i = 1; i < longueurTab; i++) {
                AH b = new AH(tabLettre[i], tabFreq[i]);
                l = l.insererOrd(b);
            }

            //On construit l arbre z a partir de la liste d AH l et on retourne ce tableau
            AH z = constr(l);
            return z;
        }

        //Si texte est vide on retourne null
        else {
            return null;
        }
    }

    public static void main(String[] args) {
        // TODO Auto-generated method stub

        //Construction de l arbre a partir d'un String donn�
        AH z = constrArbre("Le chat mange la souris. La souris est mang�e par le chat vorace. La porte de la chambre ferm�e � clef � l' int�rieur , les volets de l'unique fen�tre ferm�s , eux aussi , � l' int�rieur , et , par-dessus les volets , les barreaux intacts , des barreaux �_travers lesquels vous n' auriez pas pass� le bras ... et mademoiselle qui appelait au secours ! ... ou plut�t non , on ne l' entendait plus ... elle �tait peut-�tre morte ... mais j' entendais encore , au fond du pavillon , monsieur qui essayait d' �branler la porte ... nous avons repris notre course , la concierge et moi , et nous sommes revenus au pavillon");


        //Dictionnaire
        ArrayList<String> dictioBit = new ArrayList<>();
        ArrayList<String> dictioLetter = new ArrayList<>();
        dico(z, dictioBit, dictioLetter, "");


        //Exemples de codage de mots et de phrases
        System.out.println("On va coder la phrase suivant: ");

        //On code notre mot
        String t2 = "je crois que le code de olivier marche pas";
        String monCode = code(dictioBit, dictioLetter, t2);
        System.out.println("En codant, On obtient la chaine de bits " + monCode);

        //On decode la chaine de bit recu
        String t3 = decode(z, "1100001000110011000110001101111001101100100100001000110011111011001100011101110011101100100111011001101111111110011001011100101110000011010110101000110001111000000110011100110101011");
        System.out.println("En d�codant, On obtient le mot :" + t3);

        System.out.println();
        if (t2.equals(t3)) {
            System.out.println("T'as reussi");
        } else {
            System.out.println("T'es nul");
        }



	/*
			System.out.println("On va coder la phrase : j'aime trop l'Algorithmique");
			//On code notre mot
			monCode = code(dictioBit, dictioLetter, "j'aime trop l'Algorithmique");
			System.out.println("En codant, On obtient la chaine de bits " + monCode);

			//On decode la chaine de bit recu
			System.out.println("En d�codant, On obtient la phrase : " + decode(z, monCode));

	*/
    }

}

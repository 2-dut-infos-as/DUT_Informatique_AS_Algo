public class Liste {
    private boolean vide;
    private int tete;
    private Liste reste;

    Liste() {
        vide = true;
    }

    Liste(int val, Liste l) {
        vide = false;
        tete = val;
        reste = l;
    }

    public Liste prefixer(int val) {
        Liste li = new Liste(val, this);
        return li;
    }

    //retourne vrai si la liste est vide, faux sinon
    public boolean isVide() {
        return vide;
    }

    //Fonction affiche listePropre
    public void afficherListePropre() {
        afficherListe(this);
        System.out.println();
    }

    //Fonction afficheListe
    public static void afficherListe(Liste l) {
        if (!l.isVide()) {
            System.out.print(l.tete);
            System.out.print(" ");
            afficherListe(l.reste);
        }
    }

    //Fonction Somme
    public static int somme(Liste l) {
        if (l.isVide()) {
            return 0;
        } else return l.tete + somme(l.reste);
    }

    //Fonction Somme
    public static int sommeAlt(Liste l) {
        if (l.isVide()) {
            return 0;
        } else return l.tete + somme(l.reste);
    }


    //Fonction appartenance
    public static boolean appartenance(Liste l, int val) {
        if (l.isVide()) {
            return false;
        } else if (l.tete == val) {
            return true;
        } else return appartenance(l.reste, val);
    }

    //Fonction inserer
    public Liste insererOrd(Liste l, int val) {
        if (l.isVide()) {
            return l.prefixer(val);
        } else if (l.tete > val) {
            return (l.prefixer(val));
        } else return (insererOrd(l.getReste(), val).prefixer(l.getTete()));
    }


    public void setVide(boolean vide) {
        this.vide = vide;
    }

    public int getTete() {

        return tete;
    }

    public Liste getReste() {
        return reste;
    }

    public static void main(String[] args) {
        Liste l = new Liste();
        l = l.prefixer(5);
        l = l.prefixer(4);
        l = l.prefixer(3);
        l = l.prefixer(2);
        l.afficherListePropre();
        System.out.println(somme(l));
        System.out.println(appartenance(l, 5));
        System.out.println(appartenance(l, 6));


    }

}

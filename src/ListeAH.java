public class ListeAH {
    private boolean vide;
    private AH tete;
    private ListeAH reste;


    ListeAH() {
        vide = true;
    }

    ListeAH(AH arbre, ListeAH l) {
        vide = false;
        tete = arbre;
        reste = l;
    }

    public ListeAH prefixer(AH val) {
        ListeAH l = new ListeAH(val, this);
        return l;
    }

    //retourne vrai si la liste est vide, faux sinon
    public boolean isVide() {
        return vide;
    }

    //Fonction affiche listePropre
    public void afficherListePropre() {
        afficherListe(this);
        System.out.println();
    }

    //Fonction afficheListe
    public static void afficherListe(ListeAH l) {
        if (!l.isVide()) {
            l.getTete().AffichageInfixe();
            System.out.print(" ");
            afficherListe(l.reste);
        }
    }


    //Fonction appartenance
    public static boolean appartenance(ListeAH l, AH val) {
        if (l.isVide()) {
            return false;
        } else if (l.tete == val) {
            return true;
        } else return appartenance(l.reste, val);
    }

    //Fonction inserer
    public ListeAH insererOrd(AH val) {
        if (this.isVide()) {
            return this.prefixer(val);
        } else if (this.getTete().getFrequence() > val.getFrequence()) {
            return (this.prefixer(val));
        } else
            return (this.getReste().insererOrd(val).prefixer(this.getTete()));
    }


    public void setVide(boolean vide) {
        this.vide = vide;
    }

    public AH getTete() {

        return tete;
    }

    public ListeAH getReste() {
        return reste;
    }

    public static void main(String[] args) {
		/*
		ListeAH l=new ListeAH();

		l.afficherListePropre();
		AH a =new AH("a",4);
		AH b =new AH("b",5);
		l=l.insererOrd(b);
		l=l.insererOrd(a);
		afficherListe(l);

		*/

    }

}

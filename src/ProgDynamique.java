public class ProgDynamique {
    public static int TabFreq[] = {};

    //--------------------------------------------------------------
    //Fonction distance
    static int distance(String M, String P) {
        P = " " + P;
        M = " " + M;
        int n = M.length();
        int m = P.length();

        int Tab[][] = new int[n][m];

        int cout;

        for (int i = 0; i < n; i++) {
            Tab[i][0] = i;
        }

        for (int j = 0; j < m; j++) {
            Tab[0][j] = j;
        }

        for (int i = 1; i < n; i++) {
            for (int j = 1; j < m; j++) {

                if (M.charAt(i) == P.charAt(j)) {
                    cout = 0;
                } else {
                    cout = 1;
                }

                Tab[i][j] = minimum(Tab[i - 1][j] + 1, Tab[i][j - 1] + 1, Tab[i - 1][j - 1] + cout);
            }
        }
        for (int j = 0; j < m; j++) {
            for (int i = 0; i < n; i++) {
                System.out.print("[" + Tab[i][j] + "]" + "\t");
            }
            System.out.println();
        }

        return (Tab[n - 1][m - 1]);

    }

    //---------------------------------------------------------------------
    //Fonction distance
    static double distance_bis(String M, String P) {
        P = " " + P;
        M = " " + M;
        int n = M.length();
        int m = P.length();

        double Tab[][] = new double[n][m];

        double cout;

        for (int i = 0; i < n; i++) {
            Tab[i][0] = i;
        }

        for (int j = 0; j < m; j++) {
            Tab[0][j] = j;
        }

        for (int i = 1; i < n; i++) {
            for (int j = 1; j < m; j++) {

                if (M.charAt(i) == P.charAt(j)) {
                    cout = 0;
                } else if (M.charAt(i) == sansAccent(P.charAt(j))) {
                    cout = 0.5;
                } else {
                    cout = 1;
                }

                Tab[i][j] = minimum_bis(Tab[i - 1][j] + 1, Tab[i][j - 1] + 1, Tab[i - 1][j - 1] + cout);
            }

        }
        for (int j = 0; j < m; j++) {
            for (int i = 0; i < n; i++) {
                System.out.print("[" + Tab[i][j] + "]" + "\t");
            }
            System.out.println();
        }
        return (Tab[n - 1][m - 1]);

    }

    //-------------------------------------------------------------------
    //Fonction minimum
    static int minimum(int a, int b, int c) {
        int min = a;

        if (b < min) {
            min = b;
        }
        if (c < min) {
            min = c;
        }
        return min;
    }

    //---------------------------------------------------------------------
    //Fonction minimum_bis
    static double minimum_bis(double a, double b, double c) {
        double min = a;

        if (b < min) {
            min = b;
        }
        if (c < min) {
            min = c;
        }
        return min;
    }

    //-------------------------------------------------
    public static char sansAccent(char car) {
        final String accents = "��������������������"; // A compl�ter...
        final String letters = "AAAAAAaaaaaaEEEEeeee"; // A compl�ter...

        char c = 0;
        for (int i = 0; i < accents.length(); i++) {

            if (accents.charAt(i) == car) {
                c = letters.charAt(i);
            }
        }
        return c;
    }
    //------------------------------------------------------------------


    public static void main(String[] args) {
        // TODO Auto-generated method stub

        System.out.print(distance("CHIEN", "CHAINE"));
        System.out.println();
        System.out.print(distance_bis("chaine", "ch�in�"));
    }


}
